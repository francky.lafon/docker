# Docker

## Description

Ce repository est mon petit labo pour la partie conteneur. Dans le dossier perso, j'ai un docker-compose qui monte tout les services qui permet de récupérer les films/séries.

## Installation

Il faut Docker et Docker-compose sur la machine.

## Usage

Pour la stack Netflix on-premise:

``` bash
- docker-compose -f perso/docker-compose.yml --env-file perso/.env.prod up -d
```

## Roadmap

Il faut voir dans les issues !

## License

Apache License 2.0
